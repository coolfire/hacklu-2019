# COBOL OTP

## Challenge description

```
To save the future you have to look at the past. Someone from the inside sent you an access code to a bank account with a lot of money. Can you handle the past and decrypt the code to save the future?

Download challenge files
```

Category: crypto

Difficulty: easy

## Write-up

The challenge files contains a [COBOL file](/otp.cob) and a [out](/out) file, which appears to contain output from the program.

### Lets learn some COBOL

Since my COBOL isn't that good I installed a COBOL compiler just to make it easier to add some debug outputs. Run `cobc -x otp.cob -o otp` to build an executable from the COBOL source file.

Well, let's take a look and see if we can figure out what the COBOL program is doing. Basically this just meant a bunch of googling COBOL syntax and commands.

```cobol
file-control.
   select key-file assign to 'key.txt'
```

It looks like there should be a file called `key.txt`, presumably that's where the encryption key is read from, but it was not included with the challenge file of course.

```cobol
01 key-data pic x(50).
```

The key data is probably (at most) 50 bytes.

```cobol
01 ws-xor-len pic 9(1) value 1.
```

The variable name hints that this is probably some XOR encryption, and that it probably uses some `lenght` which is set to `1` here.

```cobol
open input key-file.
  read key-file into ws-key end-read.
```

It looks like it does indeed try to read that `key.txt` file into a variable called `ws-key`. The `01 ws-key pic x(50).` line earlier confirms that this is also at most 50 bytes.

```cobol
perform 50 times
```
This loop condition suggests that we will probably be going byte by byte over all the key bytes.

Let's take a closer look at that whole loop and see if we can confirm our suspicions.

```cobol
move 1 to ws-ctr.
perform 50 times
  call 'getchar' end-call
  move return-code to ws-parse
  move ws-parse to ws-flag

  call 'CBL_XOR' using ws-key(ws-ctr:1) ws-flag by value
  ws-xor-len end-call

  display ws-flag with no advancing
  add 1 to ws-ctr end-add
end-perform.
```

It looks like it reads a character of user input, adds it to the `ws-flag` variable and then XOR's that character with a byte from the `ws-key` variable. It then moves onto the next character and does it all over with the next character of user input.

So it looks like all we have here is just a COBOL program to do a straght XOR with a static key.

Since we only have the output, presumably we need to somehow recover the key so we can reverse the XOR and get the original input back.

### Finding the XOR key

There is a little information that will help us figure out what they key is. The first important thing to realize is that XOR is reversible. That means when you do `A XOR B` and get `C` out, you can `C XOR B` to get `A` or `C XOR A` to get `B`.

The next thing to realize is that we can probably guess what part of the original input was. Since it's a CTF, it's a reasonable guess that we are trying to recover the flag here. We know from earlier challenges that they start with `flag{` and end with `}`. That will at least give us a starting point.

The last thing that might help us is to know that encryption keys are often wrapped around to make them as long as the input. I later realized this bit of COBOL code might be a hint to that our key will be 10 bytes, wrapped 5 times, but I really don't know enough COBOL to say for sure. Either way I never realized this until after the CTF finished.

```cobol
01 ws-parse.
  05 ws-parse-data pic S9(9).
```

We can strip the first line from the `out` file, since that is just the static text line produced by the program. If we hexdump it, we do see there is a newline character at the end of the output.

```bash
$ hexdump -C out 
00000000  a6 d2 13 96 79 3b 10 64  68 75 9f dd 46 9f 5d 17  |....y;.dhu..F.].|
00000010  55 6a 68 43 8f 8c 2d 92  31 07 54 60 68 26 9f cd  |UjhC..-.1.T`h&..|
00000020  46 87 31 2a 54 7b 04 5f  a6 eb 06 a4 70 30 11 32  |F.1*T{._....p0.2|
00000030  4a 0a                                             |J.|
00000032
```

I wasn't sure if I had to strip that off or that it was part of the ciphertext, so in practice I did all of the following steps twice, once with and once without the trailing newline character.

We do also see that it's exactly 50 characters of output, so that at least matches up with what we expected from reading the COBOL source.

If we XOR the first 5 bytes of the output with `flag{` we get the bytes `\xc0\xbe\x72\xf1\x02` out, so if our guess about the start of the original plaintext is correct, that should be the first 5 bytes of the key.

Knowing that the XOR key might be shorter and just repeated a few times, I just trail-and-error tested if any sensible output was produced with shorter keys. Since I didn't know what the other key bytes would be yet, I just put 0-bytes as placeholders. After a few attempts, it looks like the shortest key that produces some sensible output is just 10 bytes long, giving us this output;

```
flag{;.dhu_c4n_.UjhCO2_c3.T`h&_s4v3*T{._fUtUr0.2J
```

We can see some fragments in there that make some sense, but if we split the output into blocks of 10 bytes it is a little easier to see what is going on

```
flag{;.dhu
_c4n_.UjhC
O2_c3.T`h&
_s4v3*T{._
fUtUr0.2J
```

The first 5 bytes we guessed are producing somewhat sensible outputs, or at least they are all printable characters, so that's a good start. Now we'll add our guess for the end of the plaintext being a `}`, making our key so far `\xc0\xbe\x72\xf1\x02\x00\x00\x00\x37\x00`, and our output so far;

```
flag{;.d_u
_c4n_.Uj_C
O2_c3.T`_&
_s4v3*T{3_
fUtUr0.2}
```

That looks good, all the other bytes which are affected by that change in the key also produced somewhat sensible outputs. At least printable characters. If all our guesswork was correct so far, that just leaves us with 4 bytes to figure out.

Let's have a closer look at the flag we have so far. We know the first underscore is correct if our last character is indeed a `}`, so there is probably some 3-letter word at there. To make the problem a little easier to approach I decided to focus on those 3 bytes first and come back to the last byte of the key stream later.

#### Brute force time

Brute forcing 3 bytes is pretty doable, but since it's just XOR, we don't really get any special feedback when we find the correct byte. But what we do know is that it should at least be a printable character. Since each byte of they key affects 5 bytes in the output, the first step is to see which key byte we can xor with every one of those output bytes to get printable characters for all 5 bytes.

```python
from pwn import *

# Each 5 bytes affected by the same byte of the key
cts = [
  "\x3b\x17\x07\x2a\x30",
  "\x10\x55\x54\x54\x11",
  "\x64\x6a\x60\x7b\x32"
]

# Quick and dirty way of creating a list with all bytes
bytelist = []
for b in range(0, 256):
  bytelist.append(chr(b))
bytelist = ''.join(bytelist)

# List of all printable characters we want to accept
testable = ''.join([string.digits, string.letters, string.punctuation])

keylist  = []

# Test if all bytes are printable when XOR'ed
for ct in cts:
  sys.stdout.write("\"")
  for b in bytelist:
    t = xor(ct, b)
    if all(c in testable for c in t):
      sys.stdout.write("\\x" + enhex(b))
      keylist.append(b)
  print "\","
```

The script also does a little output formatting so we can easily copy-paste the results into another script.

```bash
$ python2 findkeybytes.py 
"\x40\x41\x42\x43\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x50\x51\x52\x53\x54\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x79\x7a\x7b\x7c\x7d\x7e\x7f",
"\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2c\x2d\x2e\x2f\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x70\x71\x72\x73\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f",
"\x00\x01\x02\x03\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x13\x14\x16\x17\x18\x19\x1a\x1c\x1d\x1e\x41\x42\x43\x45\x46\x47\x48\x49\x4b\x4c\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5c\x5d\x5e\x5f",
```
This still gives us a pretty big list of key bytes to try, but at least it is a lot shorter already than trying all 256 possible bytes for each character. Time to write a little loop to try all our keys and see if anything interesting shows up.

```python
from pwn import *
import re

charset = [
  "\x40\x41\x42\x43\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x50\x51\x52\x53\x54\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64\x65\x66\x67\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x79\x7a\x7b\x7c\x7d\x7e\x7f",
  "\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2c\x2d\x2e\x2f\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x60\x61\x62\x63\x64\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x70\x71\x72\x73\x76\x77\x78\x79\x7a\x7b\x7c\x7d\x7e\x7f",
  "\x00\x01\x02\x03\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x13\x14\x16\x17\x18\x19\x1a\x1c\x1d\x1e\x41\x42\x43\x45\x46\x47\x48\x49\x4b\x4c\x4e\x4f\x50\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5c\x5d\x5e\x5f"
]

ciphertext = "\xa6\xd2\x13\x96\x79\x3b\x10\x64\x68\x75" \
             "\x9f\xdd\x46\x9f\x5d\x17\x55\x6a\x68\x43" \
             "\x8f\x8c\x2d\x92\x31\x07\x54\x60\x68\x26" \
             "\x9f\xcd\x46\x87\x31\x2a\x54\x7b\x04\x5f" \
             "\xa6\xeb\x06\xa4\x70\x30\x11\x32\x4a"

keystream = "\xc0\xbe\x72\xf1\x02"

for i in charset[0]:
  for j in charset[1]:
    for k in charset[2]:
      # Construct the candidate key and try it out
      cks  = ''.join([keystream, i, j, k, "\x37\x00"])
      test = xor(ciphertext, cks)

      # Print the result if we get 3 letters/numbers after flag{
      if re.match(r"^flag\{[a-zA-Z0-9]{3}", test):
        print test
```

At this point I just started looking trough the output by hand for a while to see if anything looked like it might be correct. There are still 55488 candidate solutions produced by this, but just scrolling through for a while I noticed that first word could be "now" or some variation on that, so I grepped for all candidate solutions which matched that.

```bash
$ grep -P 'flag\{[nN][oO0][wW]' candidateflags.txt 
flag{N0w_u_c4n_buy_CO2_c3rts_&_s4v3_th3_fUtUrE1!}
flag{Now_u_c4n_b*y_CO2_c3r+s_&_s4v3_+h3_fUtUrEn!}
```

Hot diggity damn! Only two options. I think we can all figure out which one is the correct one there.


"But hey!" I hear you ask. "What about that last byte?!"

Well, it seems we just got lucky and it was actually supposed to be a 0-byte. Though if it hadn't been, it would not be too hard to repeat this process once more for just that last byte.
